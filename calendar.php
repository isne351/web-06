<?php
   require_once "lib.php";
   $calendar = new draw_calendar();
   $calendar->add_event($_POST);
?>
<html>
   <head>
      <title>Week 6 Calendar</title>
      <meta charset="utf-8">
      <link href="./static/style.css" rel="stylesheet"></link>
      <link href="./static/calendar.css" rel="stylesheet"></link>
      </meta>
   </head>
   <body>
      <?php echo $calendar->draw(); ?>
   </body>
</html>