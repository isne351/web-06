<?php

	class calendar{
		
		private $event=array();
		protected $calendar;

		public function add_event($event){
			$this->event[] = $event;
		}

		protected function genarate(){

			foreach($this->event as $event){
				//รับค่าว่ากิจกรรมอยู่ในวันที่อะไร
				$date = $event['date'];
				//จำนวนวันในเดือน จากตัวแปร date 
				$number_day = date('t',strtotime($date));
				//ตัวแปรเก็บวัน
				$day = date('d',strtotime($date));
				//ตัวแปรเก็บเดือน
				$month = date('m',strtotime($date));
				//ตัวแปรเก็บ ปี
				$year = date('Y',strtotime($date));

				$this->calendar['month'] = date('F',strtotime($date));
				$this->calendar['year'] = $year;

				$week = 0;

				for($i = 1 ; $i<=$number_day; $i++){
					//เก็บว่าวันที่ 1 เป็น วันจันทร์
					$tmp = mktime(0,0,0,$month,$i,$year);
					$dow = date('w',$tmp);
					if($dow == 0){
						$week++;
					}
					$calendar_array[$week][$dow]['day'] = date('d',$tmp);

					if($i == $day){
						// เพิ่ม กิจกรรมใน อารเรย์ โดยใช้ index เป็นวันที
						$calendar_array[$week][$dow]['event'] = $event;
					}

				}

			}

			// ส่งค่ากลับไป
			return $calendar_array;
		}

	}

	class draw_calendar extends calendar{

		public function draw(){

			$now_d = date('d');
			$now_m = date('F');	

			$calendar = $this->genarate();
			//month year
			$html = '<div class="month"> <ul> <li>'.$this->calendar['month'].'<br> <span style="font-size:18px">'.$this->calendar['year'].'</span> </li> </ul> </div>';
			
			//calendar header
			$html.= '<ul class="weekdays"> <li>Mo</li> <li>Tu</li> <li>We</li> <li>Th</li> <li>Fr</li> <li>Sa</li> <li>Su</li> </ul>';

			$html.= '<ul class="days">';
			foreach($calendar as $week){
				for($dow = 0 ; $dow < 7; $dow++){
					if(isset($week[$dow]['event'])){
						$html.=$this->event_element($week[$dow]);
					}else if(isset($week[$dow]['day'])){
						if(($week[$dow]['day'] == $now_d)and($this->calendar['month'] == $now_m)){
							$html.=$this->active_element($week[$dow]);
						}else{
							$html.=$this->date_element($week[$dow]);
						}
					}else{
						$html.=$this->blank_element();						
					}
				}	
				$html.='<br>';
			}
			$html.= '</ul>';

			return $html;
		}

		private function blank_element(){
			return '<li></li>';
		}

		private function date_element($day){
			return '<li>'.$day['day'].'</li>';
		}

		private function active_element($day){
			return '<li class="active" ><a href="#" title="today">'.$day['day'].'</a></li>';
		}

		private function event_element($day){
			return '<li class="event"><a href="#" title="'.$day['event']['title'].'">'.$day['day'].'<h4>'.$day['event']['title'].'</h4><p>'.$day['event']['description'].'</p></a></li>';
		}
	}

?>